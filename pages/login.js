import React from 'react'
import {Form,Button,Card, Container} from 'react-bootstrap'



export default function login() {
    return (
        <Container>
        <Card className = "card" > 
            <Card.Body className = "cardlogin">
                <Card.Title className = "cardTitle">YVAN NAVY CORP. </Card.Title>
                <Form> 
                    <Form.Group controlId="formBasicEmail"  >
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                    </Form.Group>
                </Form>
                    <Button variant="primary" type="submit" className="buttonLogin" >
                        Submit
                    </Button>
            </ Card.Body>
        </Card> 
        </Container>
    )
}
