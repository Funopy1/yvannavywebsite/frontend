import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/login.css'

function MyApp({ Component, pageProps }) {
  return (
    <React.Fragment>
      <Component {...pageProps} />
    </React.Fragment> 
  )

}

export default MyApp
